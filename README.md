The censor (at any time, there were two) was a magistrate in ancient Rome who was responsible for maintaining the census, supervising public morality, and overseeing certain aspects of the government's finances.

The power of the censor was absolute: no magistrate could oppose his decisions, and only another censor who succeeded him could cancel those decisions.

The censor's regulation of public morality is the origin of the modern meaning of the words censor and censorship.